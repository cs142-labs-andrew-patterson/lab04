# Basic Makefile, as in my CS 14{1,2} txtbook
# (Thanks, Briggs!)

# Change to match actual program doing
PROG	:= fraction-example

SRCS	:= $(wildcard *.cpp)
OBJS	:= ${SRCS:.cpp=.o}

$(PROG):	$(OBJS)
			$(CXX) -g -o $@ $^

%.o:		%.cpp
			$(CXX) -Wall -g -c $<

clean:
			rm -f $(PROG)
			rm -f $(OBJS)
			rm -rf Debug

make.depend:	$(SRCS)
			$(CXX) -MM $^ > $@

-include make.depend
