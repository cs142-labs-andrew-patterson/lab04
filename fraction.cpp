//Source file for rational class
//Andrew Patterson

#include <iostream>
#include <cstring>
#include <cstdlib>
#include "fraction.h"

//Put read here

int Fraction::leastCommonDenominator (const Fraction &number) const
{
	int value;

	// Make two dynamic arrays - each the length of the a denominator (oh, dear, this'll be expensive)
	long int *baseArray = new long int [number.denominator_];
	long int *numberArray = new long int [denominator_];

	// Fill each list with the numbers the other denominator is a factor of
	for (int i = 0; i < number.denominator(); i++)
	{
		baseArray[i] = denominator() * (i + 1);
	}
	for (int i = 0; i < denominator(); i++)
	{
		numberArray[i] = number.denominator() * (i + 1);
	}
	// Loop through comparing them until are equal
	for (int i = 0; i < number.denominator(); i++)
	{
		for (int j = 0; j < denominator(); j++)
		{
			if (baseArray[i] == numberArray[j])
			{
				value = baseArray[i];
				break;
			}

		}
	}

	// Delete dynamic memory
	delete [] baseArray;
	delete [] numberArray;
	// Return equal value

	return value;
}

//Assignment operators
const Fraction& Fraction::operator+=(const Fraction &other)
{
	int lcd = leastCommonDenominator(other);

	numerator_ *= (lcd / denominator());
	numerator_ += other.numerator() * (lcd / other.denominator());

	denominator_ = lcd;

	simplify();

	return *this;
}

const Fraction& Fraction::operator=	(const Fraction &other)
{
	numerator_ = other.numerator();
	denominator_ = other.denominator();

	return *this;
}

const Fraction& Fraction::operator*=(const Fraction &other)
{
	numerator_ = numerator_ * other.numerator_;
	denominator_ = denominator_ * other.denominator_;
	return *this;
}

//Square brackets (basically access functions
long int Fraction::operator[] (int index) const
{
	if (index == 0)
		return numerator_;
	else if (index == 1)
		return denominator_;
	else
		throw OverflowException();
}

long int& Fraction::operator[] (int index)
{
	if (index == 0)
		return numerator_;
	else if (index == 1)
		return denominator_;
	else
		throw OverflowException();
}


void Fraction::simplify()
{
	if (denominator() == 0)
	{
		throw DivideByZeroException ();
	}
	else
	{
		long int max;
		if (denominator() < numerator())
			max = denominator() / 2;
		else
			max = numerator() / 2;
	
		int i = 2;
		while (i <= max)
		{
			for (;i <= max; i++)
			{
				if (((denominator() % i) == (numerator() % i)) && ((numerator() % i) == 0))
				{
					denominator_ = denominator_ / i;
					numerator_ = numerator_ / i;
					break;
				}
			}
			if (i > max)
				break;

			i = 2;
			
			if (denominator() < numerator())
				max = denominator() / 2;
			else
				max = numerator() / 2;
		}
	}
}

void Fraction::read (std::istream& in)
{
	const int NUMBER_LENGTH = 512;

	char buffer[NUMBER_LENGTH];
	char numerString[NUMBER_LENGTH/2];
	char denomString[NUMBER_LENGTH/2];
	std::strcpy(denomString, "0");

	//Read in string
	in >> buffer;

	int i = 0;
	//Before '/' is numerator
	for (; buffer[i] != '/' && buffer[i] != '\0'; i++)
	{
		numerString[i] = buffer[i];
	}

	if (buffer[i] == '/') { ++i; }

	for (int j = 0; buffer[i] != '\0'; j++)
	{
		denomString[j] = buffer[i];
		i++;
	}

	numerator_ = atoi(numerString);
	denominator_ = atoi(denomString);
}
