//Header file for rational class
//Andrew Patterson

#include <iostream>

#ifndef FRACTION_H
#define FRACTION_H

class Fraction
{
public:
	//Exceptions
	class OverflowException {};
	class DivideByZeroException {};

	//ctors
	Fraction(int top = 0,int bottom = 1) :
		numerator_(top), denominator_(bottom){ simplify(); };
	Fraction(const Fraction& other) :
		numerator_(other.numerator_), denominator_(other.denominator_){ simplify(); };

	//Access functions
	long int numerator() const {return numerator_;}
	long int denominator() const {return denominator_;}

	//I/O stuff
	void print(std::ostream& out = std::cout) const  {out << numerator_ << '/' << denominator_;};
	void read (std::istream& in  = std::cin);

	//Random functions for outside
	long double value() const {return double(numerator_) / double(denominator_);};
	int leastCommonDenominator (const Fraction &number) const;
	Fraction inverse() const {if (numerator() == 0) throw DivideByZeroException (); else return Fraction(denominator(), numerator());};

	//Basic arithmetic
	Fraction operator+(const Fraction &other)	const { Fraction result = *this; return result += other; };
	Fraction operator-(const Fraction &other)	const { Fraction result = *this; return result -= other; };
	Fraction operator*(const Fraction &other)	const { Fraction result = *this; return result *= other; };
	Fraction operator/(const Fraction &other)	const { Fraction result = *this; return result /= other; };
	Fraction operator-()						const { return Fraction(-numerator(), denominator()); };

	//Assignment
	const Fraction& operator= (const Fraction &other);				//done
	const Fraction& operator+=(const Fraction &other);				//done
	const Fraction& operator-=(const Fraction &other) { return *this += -other; };
	const Fraction& operator*=(const Fraction &other);				//done
	const Fraction& operator/=(const Fraction &other) { return *this *= other.inverse(); };

	//Boolean
	bool operator==(const Fraction &other) const 
		{return ((numerator() == other.numerator()) && (denominator() == other.denominator()));};
	bool operator!=(const Fraction &other) const {return !(*this == other);};
	bool operator<=(const Fraction &other) const {return (value() <= other.value());};
	bool operator>=(const Fraction &other) const {return (value() >= other.value());};
	bool operator< (const Fraction &other) const {return (value() < other.value());};
	bool operator> (const Fraction &other) const {return (value() > other.value());};

	//Increment/decrement
	const Fraction& operator++() 			{numerator_ += denominator(); return *this;};
	const Fraction& operator--() 			{numerator_ -= denominator(); return *this;};
	const Fraction& operator++(int junk)	{numerator_ += denominator(); return *this;};
	const Fraction& operator--(int junk)	{numerator_ -= denominator(); return *this;};

	//Square brackets (basically access functions)
	long int operator[] (int index) const;				//done
	long int& operator[](int index);					//done
	
	//Cast to float
	operator float() {return value();};

private:
	long int numerator_;
	long int denominator_;
	void simplify();
};

//{i,o}stream operators
inline
std::ostream& operator<< (std::ostream& out, const Fraction& foo) {foo.print(out); return out;};
inline
std::istream& operator>> (std::istream& in, Fraction& foo)  {foo.read(in); return in;};

#endif
