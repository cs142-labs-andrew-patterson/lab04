//Making a fraction / rational number class
//Andrew Patterson
//cs142, 2018-02-07 - 2018-02-

#include <iostream>
#include <cstdlib>
#include <cassert>
#include "fraction.h"

int main ()
{
	//== is tested throughout

	Fraction zeroArgCtor;				//test ctor w/ zero arguments
	assert(float(zeroArgCtor) == 0.0);	//test ctor value, casting to float

	Fraction oneArgCtor(42);			//test ctor w/ one arg
	assert(oneArgCtor.value() == 42);	//using ctor, bcs ambiguity

	Fraction fullCtor(3,4);				//Full ctor
	assert(fullCtor.value() == 0.75);

	try {
		Fraction divideByZero(1,0);
	} catch (const Fraction::DivideByZeroException&) {
		std::cout << "Divide by zero exception caught\n";
	}

	Fraction copyTest(fullCtor);		//Copy ctor
	assert(copyTest == fullCtor);

	Fraction addTest = copyTest + Fraction(5,8);//Addition, explicit constructor call
	assert(addTest == Fraction(11,8));

	Fraction subractTest = addTest - Fraction(7,3);	//Subtraction
	assert(subractTest == -Fraction(23,24));		//Testing unary -

	Fraction minEq(1);
	Fraction plusEq(1,2);
	Fraction fourth(1,4);
	minEq -= fourth;					//Testing {+,-}=
	plusEq += fourth;
	assert(minEq == plusEq);

	Fraction half(1,2);
	assert(half*half == fourth);		//Multiplication
	assert(half/Fraction(2) == fourth);	//Division

	assert(fourth != half);				//Testing !=

	assert(fourth <= half);				//Testing boolean comparisions
	assert(half >= fourth);
	assert(half >= half);
	assert(half > fourth);
	assert(fourth < half);
	
	Fraction newFourth;
	newFourth = fourth;					//Testing =
	assert(newFourth == fourth);

	Fraction thirdFourth(1,2);
	thirdFourth *= half;				//Testing *=
	assert(thirdFourth == fourth);

	Fraction yaFourth(1,2);
	yaFourth /= Fraction(2);			//Testing /=
	assert(yaFourth == fourth);

	Fraction readTest;
	std::cout << "Type in this fraction " << fourth << std::endl << '>';
	std::cin >> readTest;
	assert(readTest == fourth);

	//Test all the increment/decrement operators
	Fraction plusPlusTest(half);
	plusPlusTest++;
	assert(plusPlusTest == Fraction(3,2));
	++plusPlusTest;
	assert(plusPlusTest == Fraction(5,2));
	--plusPlusTest;
	assert(plusPlusTest == Fraction(3,2));
	plusPlusTest--;
	assert(plusPlusTest == half);

	assert(half[0] == 1);
	assert(half[1] == 2);
	try {
		int wontWork = half[2];
	} catch (const Fraction::OverflowException&) {
		std::cout << "Bracket too big exception caught\n";
	}

	system("pause");
	return 0;
}
